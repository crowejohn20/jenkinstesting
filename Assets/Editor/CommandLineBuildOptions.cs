﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEditor.Build.Reporting;

public class CommandLineBuildOptions : MonoBehaviour
{
    #region Variables    
    private static string _buildLocation = Path.GetFullPath(Path.Combine("Builds/"));
    
    #endregion

    #region BuildOptions
    
    [MenuItem("Builds/Development/iOS", false, 1)]
    public static void DebugIOS()
    {
        // Set a variable to the Documents path.
        string building = "Building Project...";
        
        DirectoryInfo docPath = Directory.CreateDirectory(_buildLocation);

        // Write the string array to a new file named "WriteLines.txt".
        using (StreamWriter outputFile = new StreamWriter(Path.Combine(docPath.FullName, "Build.txt")))
        {
            outputFile.WriteLine(building);
        }

        Debug.Log("Building Project....");

    }
    
    #endregion
}

